PROJECT_CI_TOKEN=GR1348941N7wnsCDrKcy6mj-xqSih
TEST_RUNNER_NAME=prod_terrarunner_$(date +"%d-%m-%y-%R")

gitlab-runner -l debug register --non-interactive --url https://gitlab.com --registration-token $PROJECT_CI_TOKEN \
  --executor shell --name $RUNNER_NAME --docker-pull-policy always \
  --locked=false --run-untagged=false --tag-list=prod --docker-privileged=false \
  --limit 0 \
